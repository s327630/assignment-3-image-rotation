//
// Created by klim405 on 10.11.2022.
//

#ifndef IMAGE_H
#define IMAGE_H
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>

struct pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel* data;
};

void init_image(struct image* self, size_t width, size_t height);
struct pixel* get_pixel_ptr(const struct image* image, size_t pos_from_top_px, size_t pos_from_left_px);
bool set_pixel(struct image* image, struct pixel pixel, size_t pos_from_top_px, size_t pos_from_left_px);
void deinit_image(struct image* image);

#endif
