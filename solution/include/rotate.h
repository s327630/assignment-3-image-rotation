//
// Created by klim405 on 10.11.2022.
//

#ifndef ROTATE_H
#define ROTATE_H
#include "image.h"

struct image rotate( struct image const img );

#endif
