//
// Created by klim405 on 10.11.2022.
//

#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdio.h>


struct __attribute__((packed)) bmp_header  {
    uint16_t bfType; // file signature
    uint32_t bfileSize; // file size
    uint32_t bfReserved;
    uint32_t bOffBits; // file offset to pixel array
    uint32_t biSize; // header size
    uint32_t biWidth; // image width
    uint32_t biHeight; // image
    uint16_t biPlanes; // always 1
    uint16_t biBitCount; // bit per pixel
    uint32_t biCompression; // 1 for bmp
    uint32_t biSizeImage; // image array size
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_OFFSET,
    READ_INVALID_PIXEL,
    READ_FILE_IS_NOT_OPENED
};


enum  write_status  {
    WRITE_OK = 0,
    WRITE_HEADER_ERROR,
    WRITE_PIXEL_ARRAY_ERROR,
    WRITE_FILE_IS_NOT_OPENED,
};


enum write_status write_bmp_and_free_image(char* filename, struct image* image);
enum read_status read_bmp(char* filename, struct image* image);

void print_read_status(enum read_status status);
void print_write_status(enum write_status status);

#endif

