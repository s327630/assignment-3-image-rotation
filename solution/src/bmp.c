#include "bmp.h"


static const size_t BMP_HEADER_SIZE = sizeof(struct bmp_header);
static const uint16_t SIGNATURE = 0x4d42;
static const int64_t ZERO_BYTE = 0;
static const char* const read_status_messages[] = {
        "",
        "Invalid signature",
        "Invalid bits",
        "Invalid bmp header",
        "Invalid offset in header",
        "Invalid pixel: can't read",
        "File is not opened"
};

static const char* const write_status_messages[] = {
        "",
        "Invalid bmp header",
        "Invalid pixel array",
        "File is not opened"
};

bool is_file_opened(FILE* fp) {
    return fp != NULL;
}

void print_read_status(enum read_status status) {
    if (status) fprintf(stderr, "Read error in bmp function: %s", read_status_messages[status]);
}

void print_write_status(enum write_status status) {
    if (status) fprintf(stderr, "Write error in bmp function: %s", write_status_messages[status]);
}

enum write_status check_file_for_write(FILE* fp) {
    return is_file_opened(fp) ? WRITE_OK : WRITE_FILE_IS_NOT_OPENED;
}

size_t get_padding_size(const struct image* image) {
    if ((image->width * 3) % 4) {
        return 4 - (image->width * 3) % 4;
    } else {
        return 0;
    }
}

size_t get_image_size(const struct image* image) {
    return image->height * (image->width + get_padding_size(image)) * 3;
}

size_t get_new_bmp_file_size(const struct image* image) {
    return BMP_HEADER_SIZE + get_image_size(image);
}

struct bmp_header get_bmp_header(const struct image* image) {
    struct bmp_header header = {
            .bfType = SIGNATURE,
            .bfileSize = get_new_bmp_file_size(image),
            .bfReserved = 0,
            .bOffBits = BMP_HEADER_SIZE, // file offset to pixel array
            .biSize = 40,
            .biWidth = image->width, // image width
            .biHeight = image->height, // image
            .biPlanes = 1, // always 1
            .biBitCount = 24, // bit per pixel
            .biCompression = 0,
            .biSizeImage = get_image_size(image), // image size
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
    return header;
}

enum write_status write_bmp_header(FILE* fp, const struct image* image) {
    const struct bmp_header header = get_bmp_header(image);
    if (fwrite(&header, BMP_HEADER_SIZE, 1, fp)) {
        return WRITE_OK;
    } else {
        return WRITE_HEADER_ERROR;
    }
}

void write_padding(FILE* fp, size_t padding_size) {
     fwrite(&ZERO_BYTE, 1, padding_size, fp);
 }

void write_pixel(FILE* fp, const struct pixel* pixel) {
    putc(pixel->b, fp);
    putc(pixel->g, fp);
    putc(pixel->r, fp);
}

enum write_status write_pixel_array(FILE* fp, const struct image* image) {
    size_t padding_size = get_padding_size(image);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            write_pixel(fp, get_pixel_ptr(image, i, j));
        }
        write_padding(fp, padding_size);
    }
    return ferror(fp) ? WRITE_PIXEL_ARRAY_ERROR : WRITE_OK;
}

enum write_status to_bmp(FILE* fp, const struct image* image) {
    enum write_status status;
    status = check_file_for_write(fp);
    if (status) return status;
    status = write_bmp_header(fp, image);
    if (status) return status;
    status = write_pixel_array(fp, image);
    return status;
}

enum write_status write_bmp(char* filename, struct image* image) {
    FILE* fp = fopen(filename, "wb");
    enum write_status status = to_bmp(fp, image);
    fclose(fp);
    return status;
}

enum write_status write_bmp_and_free_image(char* filename, struct image* image) {
    enum write_status status = write_bmp(filename, image);
    deinit_image(image);
    return status;
}

 enum read_status check_file_for_read(FILE* fp) {
    return is_file_opened(fp) ? READ_OK : READ_FILE_IS_NOT_OPENED;
}

enum read_status read_bmp_header(FILE* fp, struct bmp_header* header) {
    if (fread(header, BMP_HEADER_SIZE, 1, fp)) {
        return READ_OK;
    } else {
        return READ_INVALID_HEADER;
    }
}

enum read_status check_bmp_header(struct bmp_header* header) {
    if (header->bfType != SIGNATURE) return READ_INVALID_SIGNATURE;
    if (header->bOffBits >= header->bfileSize) return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status read_pixel(FILE* fp, struct pixel* pixel) {
    uint8_t color_bgr[3];
    size_t size = fread(color_bgr, 3, 1, fp);
    if (!size) return READ_INVALID_PIXEL;
    pixel->b = color_bgr[0];
    pixel->g = color_bgr[1];
    pixel->r = color_bgr[2];
    return READ_OK;
}

enum read_status skip_padding(FILE* fp, long padding_size) {
    if (fseek(fp, padding_size, SEEK_CUR)) {
        return READ_INVALID_OFFSET;
    } else {
        return READ_OK;
    }
}

enum read_status read_pixel_array(FILE* fp, struct image* image) {
    long padding_size = (long) get_padding_size(image);
    for (size_t i = 0; i < image->height; i++) {
        for (size_t j = 0; j < image->width; j++) {
            struct pixel px;
            if (read_pixel(fp, &px)) return READ_INVALID_PIXEL;
            set_pixel(image, px, i, j);
        }
        enum read_status status = skip_padding(fp, padding_size);
        if (status) return status;
    }
    return READ_OK;
}

enum read_status read_image(FILE* fp, const struct bmp_header* header, struct image* image) {
    init_image(image, header->biWidth, header->biHeight);
    if (fseek(fp, (long) header->bOffBits, SEEK_SET)) return READ_INVALID_OFFSET;
    return read_pixel_array(fp, image);
}

enum read_status from_bmp(FILE* fp, struct image* image) {
    struct bmp_header header;

    enum read_status status;
    status = check_file_for_read(fp);
    if (status) return status;
    status = read_bmp_header(fp, &header);
    if (status) return status;
    status = check_bmp_header(&header);
    if (status) return status;
    status = read_image(fp, &header, image);
    return status;
}

enum read_status read_bmp(char* filename, struct image* image) {
    FILE* fp = fopen(filename, "rb");
    enum read_status status = from_bmp(fp, image);
    fclose(fp);
    return status;
}
