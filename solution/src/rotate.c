#include "rotate.h"

struct image rotate( struct image const img ) {
    struct image rotated_img;
    init_image(&rotated_img, img.height, img.width);
    for (size_t i = 0; i < img.height; i++) {
        for (size_t j = 0; j < img.width; j++) {
            set_pixel(&rotated_img, *get_pixel_ptr(&img, i, j),
                      j, img.height-1-i);
        }
    }
    return rotated_img;
}
