#include "image.h"


void init_image(struct image* self, size_t width, size_t height) {
    if (self != NULL) {
        self->width = width;
        self->height = height;
        self->data = (struct pixel*) malloc(width * height * sizeof(struct pixel));
    }
}

void fill_image(struct image* image, size_t offset, const uint8_t* data, size_t size) {
    for (size_t i = 0; i < size/3; i++) {
        image->data[offset+i] = (struct pixel) {data[i*3], data[i*3+1], data[i*3+2]};
    }
}

bool position_in_image(const struct image* image, size_t top_px, size_t left_px) {
    return image->width > left_px && image->height > top_px;
}

struct pixel* get_pixel_ptr(const struct image* image, size_t pos_from_top_px, size_t pos_from_left_px) {
    if (position_in_image(image, pos_from_top_px, pos_from_left_px)) {
        struct pixel* pixel = image->data;
        pixel = pixel + pos_from_top_px * image->width + pos_from_left_px;
        return pixel;
    } else {
        return NULL;
    }
}


bool set_pixel(struct image* image, struct pixel pixel,
               size_t pos_from_top_px, size_t pos_from_left_px) {
    if (position_in_image(image, pos_from_top_px, pos_from_left_px)) {
        struct pixel* addr = get_pixel_ptr(image, pos_from_top_px, pos_from_left_px);
        *addr = pixel;
        return true;
    } else {
        return false;
    }
}

void deinit_image(struct image* image) {
    if (image != NULL) {
        free(image->data);
    }
}

