#include "bmp.h"
#include "rotate.h"


int main(int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning
    if (argc == 3) {
        struct image img;
        print_read_status(read_bmp(argv[1], &img));
        struct image new_img = rotate(img);
        deinit_image(&img);
        print_write_status(write_bmp_and_free_image(argv[2], &new_img));
    }
    return 0;
}
